import threading
import socket
import time


HEADER = 64
DISCONNECT = '!DISCONNECT'




class ServerSide():
    def __init__(self,port:int) -> None:
        server = socket.gethostbyname(socket.gethostname())
        self.sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        self.sock.bind((server,port))

    def listen(self):
        print('[STARTING SERVER]',flush=True)
        self.sock.listen()
        while True:
            conn, addr = self.sock.accept()
            thread = threading.Thread(target=self.handleClient,args=(conn,addr))
            thread.start()
            

    def handleClient(self, client:socket.socket, addr):
        print(f'[NEW CONNECTION] {addr} Connected',flush=True)
        self.onConnect(client)
        
        counter = [0,]
        blankCounter = 0
        thread = threading.Thread(target=self.timeout,args=(counter,client,10))
        thread.start()
        print(f'[ACTIVE CONNECTIONS] {(threading.active_count() - 1)/2}',flush=True)
        while True:
            try:
                msg = self.recv(client)
                print(msg,flush=True)                
               
                if msg == DISCONNECT:
                    print('[CLIENT DISCONNECTED] Client disconnected properly',flush=True)
                    counter[0] = 5
                    self.onDisconnect(client)
                    client.shutdown(2)
                    client.close()
                    break
                else:
                    if msg:
                        counter[0] = 0
                        blankCounter = 0
                        self.onReceive(client,msg)
                    else:
                        blankCounter +=1
                        if blankCounter > 30:
                            print('[CLIENT DISCONNECTED] Socket Not Responding',flush=True)
                            counter[0] = 5
                            self.onDisconnect(client)
                            client.shutdown(socket.SHUT_RDWR)
                            client.close()
                            break

                    
            
            except OSError as e:
                pass

            except Exception as e:
                print(e,flush=True)
                print('[CLIENT DISCONNECTED] Exception thrown above',flush=True)
                counter[0] = 5
                self.onDisconnect(client)
                client.shutdown(2)
                client.close()
                break
    
    def shutdown(self):
        self.sock.shutdown(socket.SHUT_RDWR)
        self.sock.close()

    def onConnect(self,client:socket.socket): # OVERRIDE THIS
        pass

    def onReceive(self,client:socket.socket,msg:str): # OVERRIDE THIS
        pass

    def onDisconnect(self,client:socket.socket): # OVERRIDE THIS
        pass
    
    def recv(self,client:socket.socket):
        try:
            msgLen = int(client.recv(HEADER).decode())
            msg = client.recv(msgLen).decode()
            print(f"[MESSAGE RECEIVED] {msg}",flush=True)
            return msg
        except:
            pass

    def send(self,client:socket.socket,msg:str="",msgBytes:bytes=b""):
        if msg:
            msg = msg.encode()
        else:
            msg = msgBytes
        msgLen = len(msg)
        msgLen = str(msgLen).encode()
        msgLen += b' ' * (64-len(msgLen))
        client.send(msgLen)
        client.send(msg)
    
    def timeout(self,counter:list[int],client:socket.socket,timeout:int):
        sleep = timeout/4.0
        while True:
            time.sleep(sleep)
            
            counter[0] += 1
            print(counter,flush=True)
            
            if counter[0] == 4:
                print('[CLIENT DISCONNECTED] Socket Not Responding',flush=True)
                self.onDisconnect(client)
                client.shutdown(socket.SHUT_RDWR)
                client.close()
                break
            elif counter[0] > 4:
                break
            print(counter,flush=True)


class ClientSide():
    def __init__(self,server:str,port:int) -> None:
        self.sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        self.sock.connect((server,port))
    def send(self,msg:str):
        msg = msg.encode()
        msgLen = len(msg)
        msgLen = str(msgLen).encode()
        msgLen += b' ' * (64-len(msgLen))
        self.sock.send(msgLen)
        self.sock.send(msg)
    
    def recv(self) -> str:
        try:
            msgLen = int(self.sock.recv(HEADER).decode())
            msg = self.sock.recv(msgLen).decode()
            print(f"[MESSAGE RECEIVED] {msg}",flush=True)
            return msg
        except:
            return None
        
    def shutdown(self):
        self.send(DISCONNECT)
        self.sock.shutdown(socket.SHUT_RDWR)
        self.sock.close()
    





